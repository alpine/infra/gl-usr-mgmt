package main

import (
	"context"
	"fmt"
	"log/slog"
	"os"

	"github.com/rivo/tview"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/alpine/infra/gl-usr-mgmt/ui"
)

func main() {
	err := run(
		context.Background(),
		os.Getenv,
	)

	if err != nil {
		fmt.Fprintf(os.Stderr, "FATAL: %s\n", err)
		os.Exit(1)
	}
}

func run(
	ctx context.Context,
	getenv func(string) string,
) error {
	logFile, err := os.OpenFile("app.log", os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		return fmt.Errorf("could not open app.log for writing: %w", err)
	}

	logger := slog.New(slog.NewTextHandler(logFile, &slog.HandlerOptions{
		Level: slog.LevelDebug,
	}))
	slog.SetDefault(logger)

	token := getenv("TOKEN")
	if token == "" {
		return fmt.Errorf("environment variable TOKEN not provided or empty.")
	}

	api_url := getenv("GITLAB_API")
	if api_url == "" {
		return fmt.Errorf("environment variable GITLAB_API not provided or empty.")
	}

	client, err := gitlab.NewClient(token, gitlab.WithBaseURL(api_url))
	if err != nil {
		return fmt.Errorf("Could not initialize gitlab client: %w", err)
	}

	slog.Info("Starting application")

	app := tview.NewApplication()
	controller, err := ui.NewUserController(client)
	if err != nil {
		return fmt.Errorf("could not create controller: %w", err)
	}

	app.SetRoot(controller.Page(), true)
	return app.Run()
}
