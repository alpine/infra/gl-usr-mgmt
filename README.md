# gl-usr-mgmt

<abbr title="Text user interface">TUI</abbr> to easily delete spam users on
gitlab, requires admin access.

## ⚠️ Warning

Deleting a user is non-reversible. **All** contributions of the user will be
removed as well.

## Development

To build the project, run:

```console
$ go build
```

Alternatively you can run `task build` if you use [Task](https://taskfile.dev).

To run the linting test suite, run:

```console
$ docker compose run --rm lint
```

or `task lint`.

## Setup

Provide the following environment variables:

| Variable   | Description                          |
|------------|--------------------------------------|
| TOKEN      | A gitlab token that has admin access |
| GITLAB_API | The url to the v4 gitlab API         |

## Shortcuts

| Key                                   | Description                      |
|---------------------------------------|----------------------------------|
| <kbd>k</kbd> or <kbd>Arrow up</kbd>   | Move up in the user list         |
| <kbd>j</kbd> or <kbd>Arrow down</kbd> | Move down in the user list       |
| <kbd>h</kbd> or <kbd>PgUp</kbd>       | Go to the previous page          |
| <kbd>l</kbd> or <kbd>PgDown</kbd>     | Go to the next page              |
| <kbd>d</kbd> or <kbd>Del</kbd>        | Delete user (with confirmation)  |
| <kbd>r</kbd>                          | Refresh the current page         |
| <kbd>1</kbd>                          | Go to the first page and refresh |


