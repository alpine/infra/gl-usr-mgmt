module gitlab.alpinelinux.org/alpine/infra/gl-usr-mgmt

go 1.21

require (
	github.com/gdamore/tcell/v2 v2.7.4
	github.com/hashicorp/go-retryablehttp v0.7.7
	github.com/rivo/tview v0.0.0-20240921122403-a64fc48d7654
	github.com/xanzy/go-gitlab v0.110.0
	github.com/xeonx/timeago v1.0.0-rc5
)

require (
	github.com/gdamore/encoding v1.0.0 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-runewidth v0.0.15 // indirect
	github.com/rivo/uniseg v0.4.7 // indirect
	golang.org/x/net v0.8.0 // indirect
	golang.org/x/oauth2 v0.6.0 // indirect
	golang.org/x/sys v0.20.0 // indirect
	golang.org/x/term v0.17.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	golang.org/x/time v0.3.0 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.29.1 // indirect
)
