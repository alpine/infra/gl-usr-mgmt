package util

import (
	"fmt"
	"strings"
	"unicode"
)

func EllipticalTruncate(text string, maxLen int) string {
	lastSpaceIx := maxLen
	len := 0
	for i, r := range text {
		if unicode.IsSpace(r) {
			lastSpaceIx = i
		}
		len++
		if len > maxLen {
			return text[:lastSpaceIx] + "..."
		}
	}
	// If here, string is shorter or equal to maxLen
	return text
}

func Indent(text string, indent string) string {
	buf := strings.Builder{}

	for _, line := range strings.Split(text, "\n") {
		buf.WriteString(fmt.Sprintf("%s%s\n", indent, line))
	}

	return buf.String()
}
