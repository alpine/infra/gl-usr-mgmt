package ui

import (
	"context"
	"fmt"
	"hash/crc32"
	"io"
	"log/slog"
	"strings"
	"time"

	"github.com/gdamore/tcell/v2"
	"github.com/hashicorp/go-retryablehttp"
	"github.com/rivo/tview"
	"github.com/xanzy/go-gitlab"
	"github.com/xeonx/timeago"
	"gitlab.alpinelinux.org/alpine/infra/gl-usr-mgmt/util"
)

// #############################################################################
// #############################################################################

type Project struct {
	ID                int
	Name              string
	NameWithNamespace string
	Path              string
	PathWithNamespace string
	WebUrl            string
}

func (p Project) IssueLink(iid int) string {
	return fmt.Sprintf("%s/-/issues/%d", p.WebUrl, iid)
}

func (p Project) MRLink(iid int) string {
	return fmt.Sprintf("%s/-/merge_requests/%d", p.WebUrl, iid)
}

type ContributionEvent struct {
	*gitlab.ContributionEvent
	Project Project
}

func FromGlContributionEvents(glEvents []*gitlab.ContributionEvent) []*ContributionEvent {
	events := make([]*ContributionEvent, 0, len(glEvents))

	for _, glEvent := range glEvents {
		event := &ContributionEvent{
			ContributionEvent: glEvent,
		}
		events = append(events, event)
	}

	return events
}

type User struct {
	UID          int
	Username     string
	Name         string
	Email        string
	CreatedAt    time.Time
	ConfirmedAt  time.Time
	LastActivity time.Time
	CurrentIP    string
	Blocked      bool
	State        string

	Bio          string
	Location     string
	WebsiteUrl   string
	Skype        string
	LinkedIn     string
	Twitter      string
	Organization string

	Note string

	activitiesChan <-chan []*ContributionEvent
	Activities     []*ContributionEvent
}

func (u User) FromGLUser(glUser gitlab.User) User {
	user := User{
		UID:          glUser.ID,
		Username:     glUser.Username,
		Name:         glUser.Name,
		Email:        glUser.Email,
		CreatedAt:    *glUser.CreatedAt,
		WebsiteUrl:   glUser.WebsiteURL,
		Skype:        glUser.Skype,
		LinkedIn:     glUser.Linkedin,
		Twitter:      glUser.Twitter,
		Bio:          glUser.Bio,
		Location:     glUser.Location,
		Blocked:      glUser.Locked,
		State:        glUser.State,
		Organization: glUser.Organization,
		Note:         glUser.Note,
	}
	if glUser.LastActivityOn != nil {
		user.LastActivity = time.Time(*glUser.LastActivityOn)
	}
	if glUser.ConfirmedAt != nil {
		user.ConfirmedAt = *glUser.ConfirmedAt
	}
	if glUser.CurrentSignInIP != nil {
		user.CurrentIP = (*glUser.CurrentSignInIP).String()
	}

	return user
}

// #############################################################################
// #############################################################################

type UserListModel struct {
	updateListeners []func(ctx context.Context, users []User)
	users           []User
	query           *gitlab.ListUsersOptions
}

func (ulm *UserListModel) AddUpdateListener(f func(ctx context.Context, user []User)) {
	ulm.updateListeners = append(ulm.updateListeners, f)
}

func (ulm UserListModel) notifyUpdateListeners(ctx context.Context) {
	for _, f := range ulm.updateListeners {
		f(ctx, ulm.users)
	}
}

func (ulm *UserListModel) loadUsers(
	ctx context.Context,
	userService *gitlab.UsersService,
	query *gitlab.ListUsersOptions,
	projectsService *gitlab.ProjectsService) (err error) {

	ulm.query = query
	glUsers, _, err := userService.ListUsers(query)
	if err != nil {
		return
	}

	users := []User{}
	for _, u := range glUsers {
		user := User{}.FromGLUser(*u)
		activitiesChan := make(chan []*ContributionEvent)
		user.activitiesChan = activitiesChan
		go func() {
			glActivities, _, err := userService.ListUserContributionEvents(
				user.UID,
				&gitlab.ListContributionEventsOptions{
					ListOptions: gitlab.ListOptions{PerPage: 10},
				},
			)
			if err != nil {
				slog.Error("Could not retrieve activities for user", "user", user.Username, "err", err)
				activitiesChan <- nil
				return
			}
			activities := FromGlContributionEvents(glActivities)
			projectIDs := map[int]struct{}{}
			for _, activity := range activities {
				projectIDs[activity.ProjectID] = struct{}{}
			}
			projects := map[int]gitlab.Project{}
			for projectID := range projectIDs {
				glProject, _, err := projectsService.GetProject(projectID, nil)
				if err != nil {
					slog.Error("Could not obtain project for activity", "pid", projectID)
					continue
				}
				projects[projectID] = *glProject
			}
			for _, activity := range activities {
				if project, ok := projects[activity.ProjectID]; ok {
					activity.Project = Project{
						ID:                project.ID,
						Name:              project.Name,
						NameWithNamespace: project.NameWithNamespace,
						Path:              project.Path,
						PathWithNamespace: project.PathWithNamespace,
						WebUrl:            project.WebURL,
					}
				}
			}
			activitiesChan <- activities
			close(activitiesChan)
		}()
		users = append(users, user)
	}
	ulm.users = users
	ulm.notifyUpdateListeners(ctx)
	return
}

func (ulm *UserListModel) LoadLastCreated(
	ctx context.Context,
	userService *gitlab.UsersService,
	projectsService *gitlab.ProjectsService,
) (err error) {

	query := &gitlab.ListUsersOptions{
		Active:  util.Ptr(true),
		Sort:    util.Ptr("desc"),
		OrderBy: util.Ptr("created_at"),
		ListOptions: gitlab.ListOptions{
			PerPage: 30,
			Page:    1,
		},
	}

	err = ulm.loadUsers(ctx, userService, query, projectsService)
	return
}

func (ulm *UserListModel) Delete(ctx context.Context, userService *gitlab.UsersService, user User, projectsService *gitlab.ProjectsService) (err error) {
	_, err = userService.DeleteUser(user.UID, func(r *retryablehttp.Request) error {
		urlQuery := r.URL.Query()
		urlQuery.Set("hard_delete", "true")
		r.URL.RawQuery = urlQuery.Encode()
		return nil
	})

	if err != nil {
		return fmt.Errorf("failed to delete user: %w", err)
	}

	err = ulm.Refresh(ctx, userService, projectsService)
	if err != nil {
		return fmt.Errorf("failed to refresh user list after delete: %w", err)
	}

	return
}

func (ulm *UserListModel) Refresh(ctx context.Context, userService *gitlab.UsersService, projectsService *gitlab.ProjectsService) (err error) {
	err = ulm.loadUsers(ctx, userService, ulm.query, projectsService)
	return
}

func (ulm *UserListModel) NextPage(ctx context.Context, userService *gitlab.UsersService, projectsService *gitlab.ProjectsService) (err error) {
	ulm.query.Page += 1
	err = ulm.loadUsers(ctx, userService, ulm.query, projectsService)
	return
}

func (ulm *UserListModel) PreviousPage(ctx context.Context, userService *gitlab.UsersService, projectsService *gitlab.ProjectsService) (err error) {
	if ulm.query.Page == 1 {
		return
	}

	ulm.query.Page -= 1
	err = ulm.loadUsers(ctx, userService, ulm.query, projectsService)
	return
}

func (ulm *UserListModel) FirstPage(ctx context.Context, userService *gitlab.UsersService, projectsService *gitlab.ProjectsService) (err error) {
	ulm.query.Page = 1
	err = ulm.loadUsers(ctx, userService, ulm.query, projectsService)
	return
}

// #############################################################################
// #############################################################################

type UserController struct {
	page *tview.Pages
	flex *tview.Flex

	userListView    *UserListView
	userListModel   *UserListModel
	userDetailsView UserDetailsView
}

func NewUserController(client *gitlab.Client) (uc UserController, err error) {
	ctx := context.Background()
	uc.page = tview.NewPages()

	uc.flex = tview.NewFlex()

	uc.userListModel = new(UserListModel)
	uc.userListView = NewUserListView()
	uc.userDetailsView = NewUserDetailsView()

	uc.userListModel.AddUpdateListener(uc.userListView.UpdateUsers)
	uc.userListView.AddUserSelectedListener(func(user *User) {
		uc.userDetailsView.Update(user)
	})
	uc.userListView.AddEventListener(func(ctx context.Context, event UserListEvent, data any) {
		var err error
		switch event {
		case UserListEventRefresh:
			err = uc.userListModel.Refresh(ctx, client.Users, client.Projects)
			if err != nil {
				slog.Error("could not refresh user list", "context", "AddEventListener", "event", event, "err", err)
			}
		case UserListEventNextPage:
			err = uc.userListModel.NextPage(ctx, client.Users, client.Projects)
			if err != nil {
				slog.Error("could not load next page", "context", "AddEventListener", "event", event, "err", err)
			}
		case UserListEventPreviousPage:
			err = uc.userListModel.PreviousPage(ctx, client.Users, client.Projects)
			if err != nil {
				slog.Error("could not load previous page", "context", "AddEventListener", "event", event, "err", err)
			}
		case UserListEventPageHome:
			err = uc.userListModel.FirstPage(ctx, client.Users, client.Projects)
			if err != nil {
				slog.Error("could not go to first page", "context", "AddEventListener", "event", event, "err", err)
			}
		case UserListEventDelete:
			user, ok := data.(User)
			if !ok {
				break
			}

			modal := tview.NewModal()
			modal.AddButtons([]string{"Cancel", "Delete"})
			modal.SetTitle("Confirm deleting user")
			modal.SetText(fmt.Sprintf("Are you sure you want to delete '%s'?", user.Name))
			modal.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
				if event.Key() != tcell.KeyRune {
					return event
				}

				switch event.Rune() {
				case 'j':
					return tcell.NewEventKey(tcell.KeyLeft, 0, tcell.ModNone)
				case 'k':
					return tcell.NewEventKey(tcell.KeyRight, 0, tcell.ModNone)
				}
				return event
			})
			modal.SetDoneFunc(func(buttonIndex int, buttonLabel string) {
				switch buttonLabel {
				case "Delete":
					slog.Info("Deleting user", "context", "UserController", "username", user.Username, "email", user.Email, "ip", user.CurrentIP)
					err := uc.userListModel.Delete(context.Background(), client.Users, user, client.Projects)
					if err != nil {
						slog.Error(err.Error(), "user", user.Username)
					}
				}

				uc.page.RemovePage("confirm")
			})
			uc.page.AddPage("confirm", modal, true, true)
		}
	})

	err = uc.userListModel.LoadLastCreated(ctx, client.Users, client.Projects)
	if err != nil {
		return
	}

	uc.flex.AddItem(uc.userListView.Primitive(), 0, 1, true)
	uc.flex.AddItem(uc.userDetailsView.Primitive(), 0, 2, false)

	uc.page.AddPage("users", uc.flex, true, true)
	return
}

func (uc UserController) Page() tview.Primitive {
	return uc.page
}

// #############################################################################
// #############################################################################

type UserListEventHandler func(ctx context.Context, event UserListEvent, data any)

type UserListEvent int

const (
	UserListEventRefresh UserListEvent = iota
	UserListEventNextPage
	UserListEventPreviousPage
	UserListEventPageHome
	UserListEventDelete
)

type UserListView struct {
	list  *tview.List
	users []User

	userSelectedListeners []func(user *User)
	eventListeners        []UserListEventHandler
}

func NewUserListView() (ulv *UserListView) {
	ulv = &UserListView{}
	ulv.list = tview.NewList()
	ulv.list.
		ShowSecondaryText(true).
		SetBorder(true).
		SetTitle("Users")

	ulv.list.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		switch event.Key() {
		case tcell.KeyRune:
			switch event.Rune() {
			case 'd':
				if len(ulv.users) > 0 {
					ulv.notifyEventListeners(context.Background(), UserListEventDelete, ulv.users[ulv.list.GetCurrentItem()])
				}
			case 'g':
				return tcell.NewEventKey(tcell.KeyHome, 0, tcell.ModNone)
			case 'G':
				return tcell.NewEventKey(tcell.KeyEnd, 0, tcell.ModNone)
			case 'h':
				ulv.notifyEventListeners(context.Background(), UserListEventPreviousPage, nil)
			case 'j':
				return tcell.NewEventKey(tcell.KeyDown, 0, tcell.ModNone)
			case 'k':
				return tcell.NewEventKey(tcell.KeyUp, 0, tcell.ModNone)
			case 'l':
				ulv.notifyEventListeners(context.Background(), UserListEventNextPage, nil)
			case 'r':
				ulv.notifyEventListeners(context.Background(), UserListEventRefresh, nil)
			case '1':
				ulv.notifyEventListeners(context.Background(), UserListEventPageHome, nil)
				return nil
			}
		case tcell.KeyPgDn:
			ulv.notifyEventListeners(context.Background(), UserListEventNextPage, nil)
			return nil
		case tcell.KeyPgUp:
			ulv.notifyEventListeners(context.Background(), UserListEventPreviousPage, nil)
			return nil
		case tcell.KeyDelete:
			if len(ulv.users) > 0 {
				ulv.notifyEventListeners(context.Background(), UserListEventDelete, ulv.users[ulv.list.GetCurrentItem()])
			}
			return nil
		}
		return event
	})

	ulv.list.SetChangedFunc(func(index int, mainText, secondaryText string, shortcut rune) {
		if len(ulv.users) == 0 {
			return
		}

		selectedUser := &ulv.users[index]
		ulv.notifyUserSelectedListeners(selectedUser)
	})
	return
}

func colorEmail(email string) string {
	colors := []string{
		"a45016", "aafd35", "dfe6a9", "904ace", "aa2e3f", "227b77", "7f7fd6",
		"d8fc56", "cc1431", "c9cb62", "d14a8e", "70a7e7", "76b1c8", "ebd91a",
		"18f276", "916402", "0a8ffc", "0afc77", "c8bdfc", "fcc8bd", "bdfcc8",
		"aa6305", "05aa63", "f40cfc", "2760bc", "00e28b", "e26d00", "c47a3e",
	}
	user, domain, _ := strings.Cut(email, "@")
	checksum := crc32.ChecksumIEEE([]byte(domain))
	return fmt.Sprintf("%s[#2d2d2d]@[#%s]%s[-]", user, colors[checksum%uint32(len(colors))], domain)
}

func (ulv *UserListView) UpdateUsers(ctx context.Context, users []User) {
	ulv.users = users
	currentIndex := ulv.list.GetCurrentItem()
	ulv.list.Clear()

	for _, user := range ulv.users {
		var flags []string
		if user.ConfirmedAt.IsZero() {
			flags = append(flags, "unconfirmed")
		}
		if user.Blocked {
			flags = append(flags, "blocked")
		}
		flagsRendered := ""
		if len(flags) > 0 {
			flagsRendered = fmt.Sprintf(" (%s)", strings.Join(flags, ", "))
		}
		label := fmt.Sprintf("%s%s", user.Name, flagsRendered)
		ulv.list.AddItem(label, fmt.Sprintf("  %s", colorEmail(user.Email)), 0, nil)
	}

	ulv.list.SetCurrentItem(currentIndex)
}

func (ulv UserListView) Primitive() tview.Primitive {
	return ulv.list
}

func (ulv *UserListView) AddUserSelectedListener(f func(user *User)) {
	ulv.userSelectedListeners = append(ulv.userSelectedListeners, f)
}

func (ulv *UserListView) notifyUserSelectedListeners(user *User) {
	for _, f := range ulv.userSelectedListeners {
		f(user)
	}
}

func (ulm *UserListView) AddEventListener(f UserListEventHandler) {
	ulm.eventListeners = append(ulm.eventListeners, f)
}

func (ulm UserListView) notifyEventListeners(ctx context.Context, event UserListEvent, data any) {
	for _, f := range ulm.eventListeners {
		f(ctx, event, data)
	}
}

// #############################################################################
// #############################################################################

type UserDetailsView struct {
	text *tview.TextView
}

func NewUserDetailsView() (udv UserDetailsView) {
	udv.text = tview.NewTextView()
	udv.text.SetBorder(true).SetTitle("Details")
	udv.text.SetDynamicColors(true)

	return
}

func (udv UserDetailsView) Primitive() tview.Primitive {
	return udv.text
}

func (udv UserDetailsView) Update(user *User) {
	udv.text.Clear()
	renderUserDetails(udv.text, user)
}

// #############################################################################
// #############################################################################

func renderUserDetails(w io.Writer, u *User) {
	timeagoEnglish := timeago.WithMax(timeago.English, timeago.Month, "2006-01-02")

	fmt.Fprintf(w, "[::b]Username:[::-]      [blue]%s[-] (%d)\n", u.Username, u.UID)
	fmt.Fprintf(w, "[::b]Name:[::-]          %s\n", u.Name)
	fmt.Fprintf(w, "[::b]Email:[::-]         %s\n", u.Email)
	fmt.Fprintln(w)
	fmt.Fprintf(w, "[::b]Created:[::-]       %s\n", timeagoEnglish.Format(u.CreatedAt))
	if u.ConfirmedAt.IsZero() {
		fmt.Fprintf(w, "[::b]Confirmed:[::-]     [darkgrey]Unconfirmed[-]\n")
	} else {
		fmt.Fprintf(w, "[::b]Confirmed:[::-]     %s\n", timeagoEnglish.Format(u.ConfirmedAt))
	}

	if u.LastActivity.IsZero() {
		fmt.Fprintf(w, "[::b]Last Activity:[::B] [darkgrey]None[-]\n")
	} else {
		fmt.Fprintf(w, "[::b]Last Activity:[::B] %s\n", timeagoEnglish.Format(u.LastActivity))
	}
	fmt.Fprintln(w)
	fmt.Fprintf(w, "[::b]Website:[::B]       %s\n", u.WebsiteUrl)
	fmt.Fprintf(w, "[::b]LinkedIn:[::B]      %s\n", u.LinkedIn)
	fmt.Fprintf(w, "[::b]Twitter:[::B]       %s\n", u.Twitter)
	fmt.Fprintln(w)
	fmt.Fprintf(w, "[::b]Bio:[::-]           %s\n", tview.Escape(u.Bio))
	fmt.Fprintf(w, "[::b]Location[::-]       %s\n", tview.Escape(u.Location))
	fmt.Fprintf(w, "[::b]Organization[::-]   %s\n", tview.Escape(u.Organization))
	fmt.Fprintln(w)
	fmt.Fprintf(w, "[::b]Notes:[::-]         %s\n", tview.Escape(u.Note))
	fmt.Fprintln(w)
	fmt.Fprintf(w, "[::b]Activity:[::-]\n")

	if activities, ok := <-u.activitiesChan; ok {
		u.Activities = activities
	}
	for _, activity := range u.Activities {
		switch activity.ActionName {
		case "opened":
			switch activity.TargetType {
			case "MergeRequest":
				fmt.Fprintf(w, " - Opened new merge request [:::%s]!%d[:::-] \"%s\" on %s [%s]\n", activity.Project.MRLink(activity.TargetIID), activity.TargetID, activity.TargetTitle, activity.Project.NameWithNamespace, timeagoEnglish.Format(*activity.CreatedAt))
			case "Issue":
				fmt.Fprintf(w, " - Opened new issue [:::%s]#%d[:::-] \"%s\" on %s [%s]\n", activity.Project.IssueLink(activity.TargetIID), activity.TargetIID, activity.TargetTitle, activity.Project.NameWithNamespace, timeagoEnglish.Format(*activity.CreatedAt))
			}
		case "closed":
			switch activity.TargetType {
			case "MergeRequest":
				fmt.Fprintf(w, " - Closed merge request [:::%s]!%d[:::-] \"%s\" on %s [%s]\n", activity.Project.MRLink(activity.TargetIID), activity.TargetID, activity.TargetTitle, activity.Project.NameWithNamespace, timeagoEnglish.Format(*activity.CreatedAt))
			case "Issue":
				fmt.Fprintf(w, " - Closed issue [:::%s]#%d[:::-] \"%s\" on %s [%s]\n", activity.Project.IssueLink(activity.TargetIID), activity.TargetIID, activity.TargetTitle, activity.Project.NameWithNamespace, timeagoEnglish.Format(*activity.CreatedAt))
			}
		case "pushed new":
			fmt.Fprintf(w, " - Pushed new %s %s on %s [%s]\n", activity.PushData.RefType, activity.PushData.Ref, activity.Project.NameWithNamespace, timeagoEnglish.Format(*activity.CreatedAt))
		case "pushed to":
			fmt.Fprintf(w, " - Pushed to %s %s on %s [%s]\n", activity.PushData.RefType, activity.PushData.Ref, activity.Project.NameWithNamespace, timeagoEnglish.Format(*activity.CreatedAt))
		case "deleted":
			fmt.Fprintf(w, " - Deleted %s %s on %s [%s]\n", activity.PushData.RefType, activity.PushData.Ref, activity.Project.NameWithNamespace, timeagoEnglish.Format(*activity.CreatedAt))
		case "commented on":
			fmt.Fprintf(w, " - Commented on '%s' on %s [%s]\n", activity.TargetTitle, activity.Project.NameWithNamespace, timeagoEnglish.Format(*activity.CreatedAt))
			fmt.Fprintf(w, "%s\n", util.Indent(util.EllipticalTruncate(activity.Note.Body, 80), "   "))
		case "created":
			fmt.Fprintf(w, " - Created project [:::%s]%s[:::-] [%s]\n", activity.Project.WebUrl, activity.Project.NameWithNamespace, timeagoEnglish.Format(*activity.CreatedAt))
		default:
			slog.Debug("Unhandled user activity", "user", u.Username, "action", activity.ActionName)
		}
	}
}
